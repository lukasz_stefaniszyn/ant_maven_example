package pack.me;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class TestMyMain {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test1() {
		assertTrue(new MyMain().i == 1);

	}
	
	@Test
	public void test2() {
		assertTrue(new MyMain().i == 2);

	}

	@Test
	public void test3() {
		assertTrue(new MyMain().i == 1);

	}
	
	@Test
	public void test4() {
		assertTrue(true);

	}
	
	@Test
	public void testCoverageMethod1() throws Exception {
		MyMain myMain = new MyMain();
		myMain.i = 1;
		myMain.coverageMethodTest();
		assertTrue(1 ==myMain.b);
	}
	
	@Test
	public void testCoverageMethod2() throws Exception {
		MyMain myMain = new MyMain();
		myMain.i = 20;
		myMain.coverageMethodTest();
		assertTrue(2 ==myMain.b);
	}
	
	
	
}
