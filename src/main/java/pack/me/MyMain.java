package pack.me;




/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 */

public class MyMain {
	
	public int i = 1 ;
	public int b;
	public int c;
	
	public void coverageMethodTest(){
		
		if (1 == i){
			b = 1;
		} else{
			b = 2;
		}
		return;
	}
	
	public void testIf() {
		if(true){
			c = 3;
		} else{
			c = 4;
		}
		
	}
	
}
